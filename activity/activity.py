# 1.
from abc import ABC, abstractclassmethod

class Animal(ABC):
	def eat(self, food):
		pass

	def make_sound(self):
		pass

# 2.
class Cat(Animal):
	def __init__(self, name, breed, age):
		self.name = name
		self.breed = breed
		self.age = age

	def eat(self, food):
		print(f"{self.name} wants to eat {food}")

	def make_sound(self):
		print("Miaow! Nyaw! Nyaaaaa!")

	def call(self):
		print(f"{self.name}, come on!")


class Dog(Animal):
	def __init__(self, name, breed, age):
		self.name = name
		self.breed = breed
		self.age = age

	def eat(self, food):
		print(f"Eaten {food}")

	def make_sound(self):
		print("Bark! Woof! Arf!")

	def call(self):
		print(f"Here {self.name}!")


# Test Cases
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1. call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()