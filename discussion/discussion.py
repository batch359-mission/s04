# Python Class Review

class SampleClass():
	def __init__(self, year):
		self.year = year

	def show_year(self):
		print(f"The year is: {self.year}")


myObj = SampleClass(2023)

print(myObj.year)
myObj.show_year()


# Fundamentals of OOP
# 1. Encapsulation
# 2. Inheritance
# 3. Polymorphism
# 4. Abstraction

# Encapsulation is a mechanism of wrapping the attributes and code acting on the methods together as a single unit
# ~data hiding

class Person():
	def __init__(self):
		self._name = "John Doe"
		self._age = 0

	# setter method, set_name
	def set_name(self, name):
		self._name = name

	# getter method, get_name
	def get_name(self):
		print(f"Name of a Person: {self._name}")

	# setter method, set_age
	def set_age(self, age):
		self._age = age

	# getter method, get_age
	def get_age(self):
		print(f"Age of a Person: {self._age}")

p1 = Person()
# print(p1._name)
# print(p1._name) - this is not a good practice
p1.get_name()
p1.set_name("Bob Doe")
p1.get_name()

# Mini-Activity
# Create the necessary getter and setter methods for the protected attributed called age.

# Test Cases

p1.set_age(26)
p1.get_age()


# Inheritance
# Syntax: class ChildClassName(ParentClassName)

class Employee(Person):
	def __init__(self,  employeeId):
		super().__init__()
		self._employeeId = employeeId

	# getter
	def get_employeeId(self):
		print(f"The employee ID is {self._employeeId}")

	# setter
	def set_employeeId(self, employeeId):
		self._employeeId = employeeId

	# method
	def get_details(self):
		print(f"{self._employeeId} belongs to {self._name}")

emp1 = Employee("Emp-001")
emp1.get_details()
emp1.set_name("Jane Doe")
emp1.get_details()

# Mini-Activity:
# 1. Create a new class called Student that inherits Person with the additional attributes and methods.
# attributes:
	# StudentNo, Course, Year Level

# methods:
	# get_detail: print the out put: "<Student Name> is currently in year <year_level> taking up a <course>"
	# nevessary getter and setters



class Student(Person):
	def __init__(self,  student_no, course, year_level):
		super().__init__()
		self._student_no = student_no
		self._course = course
		self._year_level = year_level

	# getter
	def get_student_no(self):
		print(f"The student no is {self._student_no}")

	# setter
	def set_student_no(self, student_no):
		self._student_no = student_no

	# getter
	def get_course(self):
		print(f"The course is {self._course}")

	# setter
	def set_student_no(self, course):
		self._course = student_course

	# getter
	def get_year_level(self):
		print(f"The year level is {self._year_level}")

	# setter
	def set_year_level(self, year_level):
		self._year_level = year_level

	# method
	def get_details(self):
		print(f"{self._name} is currently in year {self._year_level} taking up a {self._course}")

# Test Cases:
student1 = Student("stdt - 001", "Computer Science", 1)
student1.set_name("Brandon Smith")
student1.set_age(18)
student1.get_details()


# Polymorphism
# Function

class Admin():
	def is_admin(self):
		print(True)

	def user_type(self):
		print("Admin User")

class Customer():
	def is_admin(self):
		print(False)

	def user_type(self):
		print("Regular User")

# Define a test function that will take an object called obj
def test_function(obj):
	obj.is_admin()
	obj.user_type()

# Create objects instance for Admin and Customer
user_admin = Admin()
user_customer = Customer()

# Pass the created instance to the test_function
test_function(user_admin)
test_function(user_customer)


# Polymorphism with Class Methods

class TeamLead():
	def occupation(self):
		print("Team Lead")

	def hasAuth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print("Team Member")

	def hasAuth():
		print(False)

tl1 = TeamLead()
tm1 = TeamMember()

for person in (tl1, tm1):
	person.occupation()

# Polymorphism - allows us to write code that can work with objects of various classes without needing to know the specific classes/types


# Polymorphism with Inheritance
# Method Overriding

class Zuitt():
	def tracks(self):
		print("We are currently offering 3 tracks (developer career, pi-shape career and short courses)")

	def num_of_hours(self):
		print("Learn web development in 360 hours")


class DeveloperCareer(Zuitt):
	# override the parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn the basics of web development in 240 hours")

class PiShapeCareer(Zuitt):
	def num_of_hours(self):
		print("Learn skills for no-code app development in 140 hours")

class ShortCourses(Zuitt):
	def num_of_hours(self):
		print("Learn advanced topics in web development in 20 hours")


course1 = DeveloperCareer()
course2 = PiShapeCareer()
course3 = ShortCourses()

course1.num_of_hours()
course2.num_of_hours()
course3.num_of_hours()


# Abstraction
# An abstract class can be considered as a blueprint for other classes. It allows to create a set of methods that must be created within any child classes built from the abstract class
# A class which contains one or more abstract methods is called an abstract class
# Abstract classes are used to provide a common interface for a different implementations

from abc import ABC, abstractclassmethod

class Polygon(ABC):
	@abstractclassmethod
	def printNumberOfSides(self):
		pass


class Triangle(Polygon):
	def __init__(self):
		super().__init__()

	def printNumberOfSides(self):
		print("This polygon has 3 sides")

class Pentagon(Polygon):
	def __init__(self):
		super().__init__()

	def printNumberOfSides(self):
		print("This polygon has 5 sides")

shape1 = Triangle()
shape2 = Pentagon()
shape1.printNumberOfSides()
shape2.printNumberOfSides()